import sys
import time
import logging
import pickle
import argparse

import kingst_la

def main():
    parser = argparse.ArgumentParser("kingst_la - read data from Kingst Logic Analyzers")
    parser.add_argument('--debug', action='store_true', help='print debug info')
    parser.add_argument("--n-points", type=int, help="do only record given number of samples instead of all")
    parser.add_argument("--format", default="guess", help="fileformat to use. default: guess from filename, or pickle other options: ascii, raw, mat")
    parser.add_argument("--force-load-fpga-bitstream", action="store_true", help="always load the fpga bitstream at startup")
    parser.add_argument("--threshold-voltage", default=1.58, type=float, help="threshold voltage to use to device HIGH vs LOW")
    parser.add_argument("--trigger", help="trigger config. example `0:rising,1:high,3:low` ch0 on rising edge, ch1 when high and ch3 needs to be low")
    parser.add_argument("--samplerate", help="samplerate to use")
    parser.add_argument("--pre-trigger", default=0.05, type=float, help="percentage of points before trigger, default: 5")
    parser.add_argument("--set-pwm", help="enable pwm out: `pwm-id0:freq1:duty1[,pwm-od1:freq2:duty2...]`")
    parser.add_argument("filename", type=str, help="name of to save data to")

    args = parser.parse_args()

    logging.basicConfig(
        format='%(asctime)s %(levelname)s %(message)s',
        level=logging.DEBUG if args.debug else logging.INFO
    )

    conditions = []
    for cond in args.trigger.split(","):
        ch, cond = cond.split(":", 1)
        ch = int(ch)
        conditions.append((ch, cond.lower()))


    dev = kingst_la.LA2016(
        force_load_fpga_bitstream=args.force_load_fpga_bitstream
    )

    if args.set_pwm:
        pwms = []
        for pid_freq_duty in args.set_pwm.split(","):
            pid, freq, duty = pid_freq_duty.split(":")
            pid = int(pid)
            freq = float(freq)
            duty = float(duty)
            pwms.append((pid, freq, duty))
        dev.set_pwm(pwms)

    # for now always pull all 16 channels

    dev.set_threshold_voltage(args.threshold_voltage)

    dev.stop_sampling()

    dev.set_trigger_config(conditions)

    dev.set_sample_config(
        samplerate=args.samplerate,
        limit_samples=args.n_points,
        capture_ratio=args.pre_trigger / 100.
    )

    dev.start_acquisition()

    print("waiting for trigger...")
    while True:
        time.sleep(0.5)
        if dev.has_triggered():
            break
    print("got trigger.")

    # download data
    ret = dict()
    ret["xinc"] = 1. / dev.samplerate
    ret["data"] = dev.download()

    with open(args.filename, "wb") as fp:
        pickle.dump(ret, fp, protocol=pickle.HIGHEST_PROTOCOL)

if __name__ == "__main__":
    main()
