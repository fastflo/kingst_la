{'current_cell': 7, 'window_position': (987, 36), 'window_size': (1074, 681)}## nb properties ##
{'current_cell': 7, 'window_position': (987, 36), 'window_size': (1074, 681)}
## nb properties end ##
## cell 0 input ##
with open("data.raw", "rb") as fp:
    data = fp.read()
% len(data)
## cell 0 output ##
# len(data): 528
# 
## cell 0 end ##
## cell 1 input ##
% type(data)
% len(data)
## cell 1 output ##
# type(data): <class 'bytes'>
# len(data): 528
# 
## cell 1 end ##
## cell 2 input ##
TFER_PACKET_N_ACQ = 5
TFER_PACKET_SIZE = (TFER_PACKET_N_ACQ * (2 + 1) + 1) # 5 x (u16 state, u8 n_reps), u8 seq

num_tfers = len(data) / TFER_PACKET_SIZE
% num_tfers
num_tfers = int(num_tfers)
## cell 2 output ##
# num_tfers: 33.0
# 
## cell 2 end ##
## cell 3 input ##
a = time.time()
out = []
import struct
rp = 0
tfer_packets = data
for i in range(num_tfers):
    for k in range(TFER_PACKET_N_ACQ):
        acq_state = struct.unpack("<H", tfer_packets[rp:rp + 2])[0]; rp += 2
        acq_repetitions = tfer_packets[rp]; rp += 1
        out.extend([acq_state] * acq_repetitions)
    rp += 1 # seq
b = time.time() - a
print("%.3fms" % (b * 1e3))
## cell 3 output ##
# 0.678ms
# 
## cell 3 end ##
## cell 4 input ##
a = time.time()
ndata = np.frombuffer(data, dtype=np.uint8)
ndata = ndata.reshape(-1, 5 * (2 + 1) + 1)
# strip seq
ndata = ndata[:, :-1]
ndata = ndata.reshape(-1, 3)
acq_state = np.array(ndata[:, :2]).view(dtype=np.uint16)[:, 0]
acq_rep = ndata[:, 2]
out2 = np.empty((sum(acq_rep), ), dtype=np.uint16)
i = 0
for v, r in zip(acq_state, acq_rep):
    out2[i:i+r] = [v] * r
    i += r
b = time.time() - a
print("%.3fms" % (b * 1e3))
## cell 4 output ##
# 6.783ms
# 
## cell 4 end ##
## cell 5 input ##
a = time.time()
ndata = np.frombuffer(data, dtype=np.uint8).reshape(-1, 5 * (2 + 1) + 1)
ndata = ndata[:, :-1] # strip seq
ndata = ndata.reshape(-1, 3)
acq_state = np.array(ndata[:, :2]).view(dtype=np.uint16)[:, 0]
acq_rep = ndata[:, 2]
out2 = np.repeat(acq_state, acq_rep)
b = time.time() - a
print("%.3fms" % (b * 1e3))
## cell 5 output ##
# 0.284ms
# 
## cell 5 end ##
## cell 6 input ##
figure(1)
clf()
grid()
#plot(out)
plot(out2)
## cell 6 end ##
## cell 7 input ##
import pickle
with open("some", "rb") as fp:
    ret = pickle.load(fp)
xinc = ret["xinc"]
data = ret["data"]
print("freq: %.3f MHz" % (1 / xinc / 1e6))
t = arange(data.shape[0]) * xinc

figure(1)
clf()
grid()
plot(t, data)
## cell 7 output ##
# freq: 20.000 MHz
# 
## cell 7 end ##
