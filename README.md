tools to work with Kingst LA2016 logic analyzer.

for now includes a python re-implementation of what i did send to
libsigrok.

simple operation is working, firmware extraction tool still missing. you
can use those from
[sigrok-util](https://github.com/sigrokproject/sigrok-util/tree/master/firmware/kingst-la).

place firmware files (`kingst-la-01a2.fw`,
`kingst-la2016a-fpga.bitstream`) below `./firmware/` or in
`$XDG_DATA_HOME/kingst_la/` (`~/.local/share/kingst_la/`).

example:

    ./bin/kingst_la out.pickle --samplerate 20000000 --n-points 2000000 --trigger 0:rising --set-pwm 0:20:50

will set samplerate of 20 MHz, collect 2 M samples, trigger on channel 0
rising, set first pwm output to 20 Hz with 50% duty cycle.
write python-pickle into out.pickle.
