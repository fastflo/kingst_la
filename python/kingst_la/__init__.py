import os
import codecs
import time
import struct
import logging

import numpy as np
import usb.core # apt-get install python3-usb
from usb import util

log = logging.getLogger("kingst_la")

TFER_PACKET_N_ACQ = 5
TFER_PACKET_SIZE = (TFER_PACKET_N_ACQ * (2 + 1) + 1) # 5 x (u16 state, u8 n_reps), u8 seq

class LA:
    def _ctrl_in(self, bRequest, wValue, wIndex, wLength):
        return self.dev.ctrl_transfer(
            util.CTRL_TYPE_VENDOR | util.CTRL_RECIPIENT_DEVICE | util.CTRL_IN,
            bRequest, wValue, wIndex, wLength)

    def _ctrl_out(self, bRequest, wValue, wIndex, data):
        log.debug("bRequest 0x%02x, wValue 0x%02x, wIndex 0x%02x, data: %s" % (
            bRequest, wValue, wIndex, " ".join(["%02x" % b for b in data])))
        return self.dev.ctrl_transfer(
            util.CTRL_TYPE_VENDOR | util.CTRL_RECIPIENT_DEVICE | util.CTRL_OUT,
            bRequest, wValue, wIndex, data)

    def _find_data_file(self, bn, use):
        if os.path.isabs(bn):
            return bn

        locations = [
            os.path.join(os.getenv("XDG_DATA_HOME", os.path.expanduser("~/.local/share")), "kingst_la"),
            os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "firmware"),
        ]
        data_dirs = os.getenv("XDG_DATA_DIRS", "/usr/local/share:/usr/share")
        locations += [os.path.join(dir, "kingst_la") for dir in data_dirs.split(":")]

        for dn in locations:
            fn = os.path.join(dn, bn)
            if os.path.isfile(fn):
                return fn
        raise Exception("could not find %s file %r! in these dirs:\n%s" % (use, bn, "\n".join(locations)))

    def _read_data_file(self, bn, use):
        fn = self._find_data_file(bn, use)
        log.debug("reading %s from %s..." % (use, fn))
        with open(fn, "rb") as fp:
            return fp.read()

    def _load_firmware(self, fn):
        dev = usb.core.find(idVendor=self.vendor_id, idProduct=self.product_id, iProduct=0)
        if dev is None:
            raise Exception("no device Kingst LA with vendor:product id 0x%04x:0x%04x found!" % (self.vendor_id, self.product_id))
        log.info("loading firmware...")
        if fn is None:
            fn = "kingst-la-%04x.fw" % self.product_id
        firmware = self._read_data_file(fn, "firmware")

        log.debug("start loading firmware...")
        # detach kernel driver?
        if dev.is_kernel_driver_active(0):
            dev.detach_kernel_driver(0)
        dev.set_configuration(1)
        #                 bmRequestType,         bRequest, wValue=0, wIndex=0, data_or_wLength=None, timeout=None
        dev.ctrl_transfer(util.CTRL_TYPE_VENDOR, 0xa0, 0xe600, 0, bytes([1])) # cpu reset on
        # todo: use _ctrl_out!
        offset = 0
        while offset < len(firmware):
            to_send = min(len(firmware) - offset, 4 * 1024)
            dev.ctrl_transfer(util.CTRL_TYPE_VENDOR, 0xa0, offset, 0, firmware[offset:offset + to_send])
            offset += to_send
        dev.ctrl_transfer(util.CTRL_TYPE_VENDOR, 0xa0, 0xe600, 0, bytes([0])) # cpu reset off
        del dev
        dev = None

        # wait for device to come back with iProduct == 2
        log.debug("loading firmware finished. expect device to reset now.")
        dl = time.time() + 5
        while dev is None:
            time.sleep(1)
            dev = usb.core.find(idVendor=self.vendor_id, idProduct=self.product_id, iProduct=2)
            if not dev:
                log.debug("device did not yet reappear after firmware load...")
            if time.time() > dl:
                raise Exception("device failed to reappear after firmware load!")
        log.debug("successfully loaded firmware.")
        return dev

    def _set_run_mode(self, mode):
        CTRL_RUN = 0x00
        self._ctrl_out(0x20, CTRL_RUN, 0, bytes([mode]))
        log.debug("did set run_mode 0x%02x" % mode)

    def _get_run_state(self):
        CTRL_RUN = 0x00
        state = struct.unpack("<H", self._ctrl_in(0x20, CTRL_RUN, 0, 2))[0]
        log.debug("run_state: 0x%04x" % state)
        return state

    def _load_fpga_bitstream(self, fn):
        if fn is None:
            fn = "kingst-la2016a-fpga.bitstream"
        bitstream = self._read_data_file(fn, "fpga bitstream")
        log.debug("uploading fpga bitstream...")
        self._ctrl_out(0x50, 0x00, 0, struct.pack("<I", len(bitstream)))
        org_bitstream_len = len(bitstream)
        zero_pad_to = 0x2c000
        if len(bitstream) < zero_pad_to:
            bitstream += bytes([0] * (zero_pad_to - len(bitstream)))
        pos = 0
        while pos < len(bitstream):
            to_send = min(len(bitstream), 4096)
            ret = self.dev.write(2, bitstream[pos:pos + to_send])
            if to_send != ret:
                log.warning("expected to send %d bytes, ret is %d" % (to_send, ret))
                if ret > 0:
                    to_send = ret
                else:
                    Exception("did not get out any bytes!")
            pos += to_send
        log.debug("upload of fpga bitstream done.")

        cmd_resp = self._ctrl_in(0x50, 0x00, 0, 1)[0]
        if cmd_resp != 0:
            raise Exception("after fpga bitstream upload command response is 0x%02x, expect 0!" % cmd_resp)
        time.sleep(0.030)
        self._ctrl_out(0x10, 0x01, 0, b"")
        time.sleep(0.040)

        if self._get_run_state() == 0xffff:
            raise Exception("run_state after fpga bitstream upload is 0xffff!");

        if org_bitstream_len == 0x2b602:
            # v3.4.0
            unknown_cmd1 = bytes( [ 0xa3, 0x09, 0xc9, 0x8d, 0xe7, 0xad, 0x7a, 0x62, 0xb6, 0xd1, 0xbf ] )
            expected_unknown_resp1 = bytes( [ 0xa3, 0x10, 0xda, 0x66, 0x6b, 0x93, 0x5c, 0x55, 0x38, 0x50, 0x39, 0x51, 0x98, 0x86, 0x5d, 0x06, 0x7c, 0xea ] )
        else:
            # v3.4.2
            if org_bitstream_len != 0x2b839:
                log.warning("the FPGA bitstream size %d is unknown. tested bistreams from vendor's version 3.4.0 and 3.4.2" % (org_bitstream_len, ))
            unknown_cmd1 = bytes( [ 0xa3, 0x09, 0xc9, 0xf4, 0x32, 0x4c, 0x4d, 0xee, 0xab, 0xa0, 0xdd ] )
            expected_unknown_resp1 = bytes( [ 0xa3, 0x10, 0xb3, 0x92, 0x7b, 0xd8, 0x6b, 0xca, 0xa5, 0xab, 0x42, 0x6e, 0xda, 0xcd, 0x9d, 0xf1, 0x31, 0x2f ] )

        self._ctrl_out(0x60, 0x00, 0, unknown_cmd1)
        time.sleep(0.080)
        unknown_resp1 = self._ctrl_in(0x60, 0x00, 0, len(expected_unknown_resp1))
        unknown_resp1 = bytes(unknown_resp1)
        #if unknown_resp1 != expected_unknown_resp1:
        #    log.warning("unknown_cmd1 response is not as expected...\nreceived: %r\nexpected: %r" % (
        #        unknown_resp1,
        #        expected_unknown_resp1))

        state = self._get_run_state()
        if state != 0x85e9:
            raise Exception("expect run state to be 0x85e9, but it reads 0x%04x" % state)

        unknown_cmd2 = bytes( [ 0xa3, 0x01, 0xca ] )
        expected_unknown_resp2 = bytes( [ 0xa3, 0x08, 0x06, 0x83, 0x96, 0x29, 0x15, 0xe1, 0x92, 0x74, 0x00, 0x00 ] )
        self._ctrl_out(0x60, 0x00, 0, unknown_cmd2)
        time.sleep(0.080)
        unknown_resp2 = self._ctrl_in(0x60, 0x00, 0, len(expected_unknown_resp2))
        unknown_resp2 = bytes(unknown_resp2)
        if unknown_resp2 != expected_unknown_resp2:
            log.warning("unknown_cmd2 response is not as expected...\nreceived: %r\nexpected: %r" % (
                unknown_resp2,
                expected_unknown_resp2))

        self._ctrl_out(0x38, 0x00, 0, b"")
        log.debug("device should now be initialized")


    def _init_device(self, force_load_fpga_bitstream=False):
        self.dev = usb.core.find(idVendor=self.vendor_id, idProduct=self.product_id, iProduct=2)
        if self.dev is None:
            log.debug("no device 0x%04x:0x%04x found -- check whether firmware needs loading..." % (self.vendor_id, self.product_id))
            self.dev = self._load_firmware(self.firmware_file)
            self._had_to_load_firmware = True
        else:
            log.debug("firmware was already loaded")
            self._had_to_load_firmware = False
        self.dev.set_interface_altsetting(0, 0)

        i1 = self._ctrl_in(0xa2, 0x20, 0, 4)
        i1 = struct.unpack("<I", i1)[0]
        log.debug("i1: 0x%08x" % i1)

        i2 = self._ctrl_in(0xa2, 0x08, 0, 4 * 2)
        i21, i22 = struct.unpack("<II", i2)
        log.debug("i2: 0x%08x, 0x%08x" % (i21, i22))

        if self._had_to_load_firmware or force_load_fpga_bitstream:
            self._load_fpga_bitstream(self.bitstream_file)

    def set_threshold_voltage(self, voltage):
        if voltage not in self.logic_thresholds:
            raise Exception("can not use threshold_voltage %f, choose one of %r" % (self.logic_thresholds, ))
        o1 = 15859969; v1 = 0.45;
        o2 = 15860333; v2 = 1.65;
        f = (o2 - o1) / (v2 - v1);
        cfg = struct.pack("<I", int(o1 + (voltage - v1) * f))

        CTRL_THRESHOLD = 0x48
        self._ctrl_out(0x20, CTRL_THRESHOLD, 0, cfg)
        self.threshold_voltage = voltage

    def set_trigger_config(self, conditions):
        """

        conditions is a list of 2-tuples of a channel-id and one of
        `rising`, `falling`, `one` or `zero`
        """
        channels = 0xffff
        enabled = 0
        level = 0
        high_or_falling = 0

        for ch, cond in conditions:
            mask = 1 << ch

            if cond in ("one", "high", "1"):
                level |= mask
                high_or_falling |= mask
            elif cond in ("zero", "low", "0"):
                level |= mask
                high_or_falling &= ~mask
            elif cond == "rising":
                if enabled & ~level:
                    raise Exception("can only have one edge-trigger active!")
                level &= ~mask
                high_or_falling &= ~mask
            elif cond == "falling":
                if enabled & ~level:
                    raise Exception("can only have one edge-trigger active!")
                level &= ~mask
                high_or_falling |= mask
            else:
                raise Exception("invalid trigger condition %r for channel %r" % (cond, ch))
            enabled |= mask

        cfg = struct.pack(
            "<IIII",
            channels,
            enabled,
            level,
            high_or_falling
        )
        log.debug("channels: 0x%04x, enabled: 0x%04x, level: 0x%04x, high_or_falling: 0x%04x" % (
            channels,
            enabled,
            level,
            high_or_falling))
        CTRL_TRIGGER = 0x30
        self._ctrl_out(0x20, CTRL_TRIGGER, 16, cfg)

    def set_pwm(self, pwms):
        CTRL_PWM = [ 0x70, 0x78 ]
        MAX_PWM_FREQ = 20e6
        PWM_CLOCK = 200e6

        not_mentioned = list(range(self.n_pwm))
        for pid, freq, duty in pwms:
            if pid >= self.n_pwm or pid < 0:
                raise Exception("invalid pwm-id %r, choose one of %r" % (pid, list(range(self.n_pwm))))
            if freq > MAX_PWM_FREQ or freq < 0:
                raise Exception("pwm%d freq %r is too high! stay below %r!" % (pid, MAX_PWM_FREQ))
            if duty < 0 or duty > 100:
                raise Exception("pwm%d duty %r is out of range 0..1!" % (pid, duty))
            period = int(PWM_CLOCK / freq)
            duty = int(round(period * duty / 100.))
            cfg = struct.pack("<II", period, duty)
            self._ctrl_out(0x20, CTRL_PWM[pid], 0, cfg)
            not_mentioned.remove(pid)
        for pid in not_mentioned:
            cfg = struct.pack("<II", 0, 0)
            self._ctrl_out(0x20, CTRL_PWM[pid], 0, cfg)

    def set_sample_config(self, samplerate=None, limit_samples=None, capture_ratio=0.1):
        total = 128 * 1024 * 1024
        MAX_SAMPLE_RATE = self.samplerates[-1]

        if samplerate is None:
            samplerate = MAX_SAMPLE_RATE
        if limit_samples is None:
            limit_samples = self.max_sample_depth

        samplerate = int(samplerate)
        if samplerate > MAX_SAMPLE_RATE:
            raise Exception("too high sample rate: %r, max is %r" % (samplerate, MAX_SAMPLE_RATE))

        clock_divisor = round(MAX_SAMPLE_RATE / samplerate)
        if clock_divisor > 0xffff:
            clock_divisor = 0xffff
        self.samplerate = MAX_SAMPLE_RATE / clock_divisor

        if limit_samples > self.max_sample_depth:
            raise Exception("too high sample depth: %r, max is %r" % (limit_samples, self.max_sample_depth))

        pre_trigger_size = int((capture_ratio * limit_samples) / 100)
        psa = pre_trigger_size * 256
        self.pre_trigger_size = pre_trigger_size * 100

        sample_config = struct.pack(
            "<IIHIH",
            limit_samples,
            psa & 0xffffffff,
            (psa >> 32) & 0xffff,
            int((total * capture_ratio) / 100),
            clock_divisor
        )
        CTRL_SAMPLING = 0x20
        self._ctrl_out(0x20, CTRL_SAMPLING, 0, sample_config)

    def stop_sampling(self):
        self._ctrl_out(0x20, 0x03, 0, b"\x00")
    def start_acquisition(self):
        self._set_run_mode(3)
        log.debug("started acquisition")
    def stop_acquisition(self):
        self._set_run_mode(0)
        log.debug("stopped acquisition")
    def has_triggered(self):
        state = self._get_run_state()
        return (state & 0x3) == 1

    def download(self):
        # get capture info
        CTRL_BULK = 0x10
        capture_info_format = "<III"
        capture_info = self._ctrl_in(0x20, CTRL_BULK, 0, struct.calcsize(capture_info_format))
        n_rep_packets, n_rep_packets_before_trigger, write_pos = struct.unpack(capture_info_format, capture_info)
        log.debug("capture info: n_rep_packets %d, n_rep_packets_before_trigger %d, write_pos %d" % (
            n_rep_packets, n_rep_packets_before_trigger, write_pos))

        if n_rep_packets % TFER_PACKET_N_ACQ:
            log.warning("number of packets is not as expected multiples of %d: %d" % (TFER_PACKET_N_ACQ, n_rep_packets))

        n_transfer_packets_to_read = int(n_rep_packets / TFER_PACKET_N_ACQ)
        n_bytes_to_read = n_transfer_packets_to_read * TFER_PACKET_SIZE
        read_pos = write_pos - n_bytes_to_read
        n_reps_until_trigger = n_rep_packets_before_trigger

        log.debug("want to read %d tfer-packets starting from pos %d",
                  n_transfer_packets_to_read, read_pos)

        self._ctrl_out(0x38, 0x00, 0, b"")

        bulk_cfg = struct.pack(
            "<II",
            read_pos,
            n_bytes_to_read
        )
        log.debug("will read from 0x%08x, 0x%08x bytes" % (read_pos, n_bytes_to_read))
        self._ctrl_out(0x20, CTRL_BULK, 0, bulk_cfg)
        self._ctrl_out(0x30, 0x00, 0, b"")


        BULK_IN_EP = 0x86

        all_data = []

        while n_bytes_to_read > 0:
            to_read = n_bytes_to_read
            BULK_MAX = 8388608 # todo: make this smaller for MXE build?
            if to_read > BULK_MAX:
                to_read = BULK_MAX
            data = self.dev.read(BULK_IN_EP, to_read)
            all_data.append(data)
            log.debug("got %d bytes of data via bulk-read" % len(data))
            n_bytes_to_read -= len(data)
        data = b"".join(all_data)
        log.debug("received all %d bytes" % len(data))

        self.stop_acquisition() # keep?

        return self._decode(data)

    def _decode(self, data):
        ndata = np.frombuffer(data, dtype=np.uint8).reshape(-1, TFER_PACKET_SIZE)
        ndata = ndata[:, :-1] # strip `seq`
        ndata = ndata.reshape(-1, 3)
        acq_state = np.array(ndata[:, :2]).view(dtype=np.uint16)[:, 0] # select `state`
        acq_rep = ndata[:, 2] # select `repetition`
        return np.repeat(acq_state, acq_rep)

class LA2016(LA):
    vendor_id = 0x77a1
    product_id = 0x01a2

    samplerates = list(map(int, [
        20e3, 50e3, 100e3, 200e3, 500e3,
        1e6, 2e6, 4e6, 5e6, 8e6, 10e6, 20e6, 50e6, 100e6, 200e6
    ]))

    n_channels = 16
    n_pwm = 2

    logic_thresholds = [ 1.58, 2.5, 1.165, 1.5, 1.25, 0.9, 0.75, 0.60, 0.45 ]

    max_sample_depth = int(10e9)

    def __init__(self, firmware_file=None, bitstream_file=None, force_load_fpga_bitstream=False):
        LA.__init__(self)

        self.firmware_file = firmware_file
        self.bitstream_file = bitstream_file
        self._init_device(force_load_fpga_bitstream=force_load_fpga_bitstream)
